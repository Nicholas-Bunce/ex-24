﻿using System;

namespace ex_24
{
    class Program
    {
                public static void FirstMethod()
        {
                    var a = $"This is my first method";
                    Console.WriteLine(a);
        }
                public static void Myname(string name)
        {
                Console.WriteLine($"Hello my name is {name}");
        }    
                public static string Name(string name)
                {
                    return $"My name is {name}";
                }
                public static string Multiply(int num1, int num2, int num3)
                {
                    return $"Mutiply these numbers {num1} x {num2} x {num3} ={num1 * num2 * num3}";
                }
                public static void LoopAdd(int num4)
                {
                 
                    for (var i = 0; i <num4; i++)
                    {
                        var a = i + 1;
                        Console.WriteLine($"this number is not the number {a} you want");
                    }    
                }

                
                public static void Main(string[] args)
        {        
                FirstMethod();
                FirstMethod();
                FirstMethod();
                FirstMethod();
                FirstMethod();
                Console.WriteLine("==========================================");
                Myname("Nick");
                Myname("Towley");
                Myname("Jess");
                Myname("Bob");
                Myname("Rod");
                Console.WriteLine("==========================================");
                Console.WriteLine(Name("Sod"));
                Console.WriteLine(Name("Todd"));
                Console.WriteLine(Name("Rob"));
                Console.WriteLine(Name("Dobbie"));
                Console.WriteLine(Name("Bobby"));
                Console.WriteLine("==========================================");
                Console.WriteLine(Multiply(10, 25, 4));
                Console.WriteLine(Multiply(44, 30, 0));
                Console.WriteLine(Multiply(58, 2, 19));
                Console.WriteLine("==========================================");
                LoopAdd(100);
                
        }
     
            
    }
}
